# Supermarket App Uji Kompetensi

Dalam proyekan ini, sudah tersedia data migrataion dan data seed. Jadi tinggal jalankan perintah **migrate** dan atau **db:seed** untuk migrasi dan tambah data user.

## Route List

- Login (auth/login dengan parameter email dan password)
- List Produk (auth/product/list dengan parameter Authorization dengan type bearer)
- Store Produk (auth/product/store dengan parameter Authorization dengan type bearer, name, dan qty)
- Update Produk (auth/product/update dengan parameter Authorization dengan type bearer, name, dan qty)
- Approve Produk (auth/product/approve dengan parameter Authorization dengan type bearer, approve (1 atau 0), dan description)
- Reject Produk (auth/product/reject dengan parameter Authorization dengan type bearer, approve (1 atau 0),reject (1 atau 0), dan description)


## List User
- Role : Supervisor => email : ridowann@gmail.com , password = password
- Role : Staff => email : staff@gmail.com, password = password
- Role : Cashier => email : cashier@gmail.com, password = password
