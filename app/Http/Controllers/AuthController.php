<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

	public function login(Request $request)
	{

		$input = $request->all();

		//validate incoming request 
		$validationRules = [
			'email' => 'required|string',
			'password' => 'required|string',
		];

		$validator = \Validator::make($input, $validationRules);

		if($validator->fails()){
			return response()->json(['status' => 'error', 'data' => [$validator->errors()],],400);
		}

		$credentials = $request->only(['email', 'password']);

		if (! $token = Auth::attempt($credentials)) {			
			return response()->json(['status'=>'fail','message' => 'Unauthorized'], 401);
		}

		//return $this->respondWithToken($token);
		// Auth::user()->id;
		return response()->json([
			'status' 		=> 'succes',
			'data' => [
				'token'			=> $token,
				'token_type'	=> 'bearer',
				'expire_in'		=> Auth::factory()->getTTL() * 60
			],
		], 200);
	}

	public function logout(Request $request)
	{
		Auth::guard('api')->logout();

		return response()->json(['status' => 'success','data' => ['message'=>'token removed'],],200) ;
    }

}